# Cloud Network



## Getting started
- [] Install python3 and pip
- [] pip install slack
- [] pip install slackclient
- [] pip install python-dotenv
- [] pip install flask
- [] pip install pyjwt

## Add your files
- [] Add .env file
- [] Add header file extension and follow setting (Reference -> Setting Header file extension)
{
    "workbench.colorTheme": "Default Dark+",
    "fileHeaderComment.parameter":{

        "*":{
            "author": "Thien Vu",
            "Company" : "1500$",
            "license_mit":[
                "The License (${Company})",
                " Copyright (c) ${year} ${author}",
                "",
                " Project: E-Commercial",
                " Permission is hereby granted, free of charge, to any person obtaining a copy of this software",
                " and associated documentation files (the \"Software\"), to deal in the Software without restriction,",
                " including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,",
                " and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,",
                " subject to the following conditions:",
                "",
                " The above copyright notice and this permission notice shall be included in all copies or substantial",
                " portions of the Software.",
            ]
        }
    },
    "fileHeaderComment.template":{
        
        "mit":[
            "${commentbegin}",
            "${commentprefix} Created on ${date}",
            "${commentprefix}",
            "${commentprefix} ${license_mit}",
            "${commentend}"
        ]
    }
}

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:799d0191a4aeeaefb42ba80f131bacd6?https://gitlab.com/htduy7198/cloud-network/-/settings/integrations)

