from flask import Blueprint
from .ServerController import FirebaseAPI, app

auth = Blueprint('auth', __name__)
firebase_view = FirebaseAPI.as_view('firebase_api')


# add Rules for API Endpoints
auth.add_url_rule(
    '/api/trace-link-from-firebase',
    view_func=firebase_view,
    methods=['POST']
)

app.register_blueprint(auth)
