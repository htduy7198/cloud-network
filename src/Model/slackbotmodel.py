#
# Created on Tue Jan 18 2022
#
# The License (Your Company)
# Copyright (c) 2022 Thien Vu
#
# Project: E-Commercial
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.

from Constants import slackConst
from cgitb import text
from lib2to3.pgen2 import token
from aiohttp import client
import requests
import slack


class SlackModel:

    def __init__(self) -> None:
        pass

    # Sending test normal message in Slack
    def sendTestMessage(self):
        client = slack.WebClient(token=slackConst.SLACK_BOT_TOKEN)
        client.chat_postMessage(
                channel="#project_report",
                color='#00FF00',
                blocks=[
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": "Danny Torrence left the following review for your property:"
                        }
                    }
                ],
        )

    # send message to Slack
    def sendMessageToSlack(self, color, message, url, platform):
        requests.post(slackConst.SLACK_URL,
                    headers={"Content-Type": "application/json"},
                    json={
                        "blocks": [
                                {
                                    "type": "section",
                                    "fields": [
                                        {
                                            "type": "mrkdwn",
                                            "text": f'*{message}*'
                                        }
                                    ]
                        }],
                        "attachments": [
                                {
                                    "color": f'{color}',
                                    "blocks": [
                                        {
                                            "type": "section",
                                            "fields": [
                                                {
                                                    "type": "mrkdwn",
                                                    "text": f"*Summary*\n{url}"
                                                },
                                                {
                                                    "type": "mrkdwn",
                                                    "text": "*App*\nE-Commercial"
                                                }
                                            ]
                                        },
                                        {
                                            "type": "section",
                                            "fields": [
                                                {
                                                    "type": "mrkdwn",
                                                    "text": f"*Platform*\n{platform}"
                                                },
                                                {
                                                    "type": "mrkdwn",
                                                    "text": "*Version*\n1.0"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]   
                    }
                    )
