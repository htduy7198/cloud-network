#
# Created on Wed Jan 19 2022
#
# The License (Your Company)
# Copyright (c) 2022 Thang Cao
#
# Project: E-Commercial
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
#

from .slackbotmodel import SlackModel
from Constants import slackConst
from flask import Flask, request, Response, make_response, jsonify
from flask.views import MethodView
import jwt


model = SlackModel()
app = Flask(__name__)
app.config['SECRET_KEY'] = 'your secret key'

class FirebaseAPI(MethodView):
    def post(self):
        try:
            json_data = request.get_json()
            response_object = {"status": "Success",
                               "message": "Successfully sent message to slack"}
            if json_data.get("link"):
                print ("pass")
                # model.sendMessageToSlack(slackConst.COLOR, "1500$ Target App E-Commercial", json_data["link"], "IOS")
            else:
                response_object = {"status": "Failed",
                                   "message": "Please provide \"link\" key in JSON body"}
                return make_response(jsonify(response_object)), 400
        except KeyError as e:
            response_object = {"status": "Failed",
                               "message": "Request contains invalid JSON body {}:".format(str(e))}
            return make_response(jsonify(response_object)), 400
        except Exception:
            response_object = {"status": "Failed",
                               "message": "Invalid request"}
            return make_response(jsonify(response_object)), 400
        return make_response(jsonify(response_object)), 200


class JwtAuthenticate(MethodView):
    def get(self):
        json_data = request.get_json()
        payload_data = {}
        if json_data.get("username"):
            payload_data.update({"username":json_data.get("username")})
        if json_data.get("number"):
            payload_data.update({"number": json_data.get("number")})
        payload_data = jwt.encode(payload=payload_data, key=app.config['SECRET_KEY'])
        print (payload_data)
        #continue

    def post(self):
        pass

