#
# Created on Tue Jan 18 2022
#
# The License (Your Company)
# Copyright (c) 2022 Thien Vu
#
# Project: E-Commercial
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.

import os
from pathlib import Path
from dotenv import load_dotenv

# Constants in project
COLOR = '#00FF00'
SLACK_URL = 'https://hooks.slack.com/services/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'

# Your app's Slack bot user token
env_path = Path(__file__).parent / '.env'
load_dotenv(dotenv_path=env_path)
SLACK_BOT_TOKEN = os.environ["SLACK_TOKEN"]
JWT_BOT_SECRET_KEY = os.environ["SECRET_KEY"]
