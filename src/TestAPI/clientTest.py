import requests
link = {"link": "https://api.slack.com/messaging/composing/layouts"}


class ServicePostLinkApiError(Exception):
    pass


def test_post_link():
    response = requests.post("http://127.0.0.1:80/api/trace-link-from-firebase", json=link)
    message_response = "".join([response.text, "\n", str((response.status_code, response.reason))])
    if response.ok:
        print(message_response)
    else:
        print(message_response)
        raise ServicePostLinkApiError


test_post_link()




